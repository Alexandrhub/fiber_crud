package main

import (
	"log"

	"gitlab.com/Alexandrhub/fiber_crud/app"
)

func main() {
	err := app.Run()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Server started on port 8080")
}
