package database

import (
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var db *mongo.Database

func New() error {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		return fmt.Errorf("you must set your 'MONGODB_URI' environmental variable")
	}

	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))
	if err != nil {
		return err
	}

	db = client.Database("test")

	err = db.Client().Ping(context.Background(), readpref.Primary())
	if err != nil {
		return err
	}

	return nil
}

func CloseDB() error {
	return db.Client().Disconnect(context.Background())
}

func GetDBCollection(col string) *mongo.Collection {
	return db.Collection(col)
}
