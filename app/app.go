package app

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
	"gitlab.com/Alexandrhub/fiber_crud/database"
	"gitlab.com/Alexandrhub/fiber_crud/internal/router"
)

func Run() error {
	err := loadEnv()
	if err != nil {
		return err
	}

	err = database.New()
	if err != nil {
		return err
	}

	app := fiber.New()

	// basic middlewares
	app.Use(logger.New())
	app.Use(recover.New())
	app.Use(cors.New())

	router.AddBookGroup(app)
	app.Listen(":8080")

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-done
		log.Println("Server shutting down")
		_ = app.Shutdown()
	}()

	defer Close()

	return nil
}

func Close() error {
	return database.CloseDB()
}

func loadEnv() error {
	if err := godotenv.Load(); err != nil {
		return err
	}

	return nil
}
