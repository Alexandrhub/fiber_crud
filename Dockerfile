# Используем официальный образ Go как базовый
FROM golang:1.21-alpine as builder

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем исходники приложения в рабочую директорию
COPY ./ ./
# Скачиваем все зависимости
RUN go mod tidy
# Собираем приложение
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd/fiber-crud/main.go

# Начинаем новую стадию сборки на основе минимального образа
FROM alpine:latest

# Добавляем исполняемый файл из первой стадии в корневую директорию контейнера
COPY --from=builder /app/main /main
COPY ./.env ./.env
EXPOSE 3000
EXPOSE 8081
EXPOSE 27017

# Запускаем приложение
CMD ["/main"]